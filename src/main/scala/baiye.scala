import Math._

object baiye {

    case class FacebookPost(text: String)

    def main(args: Array[String]) {
        splitEachText.foreach((x) => println(x))
    }

    def loadDataSet():List[(FacebookPost, Int)] = 
        List(
            FacebookPost("My dog has flea problems, how do I help"),
            FacebookPost("Dogs are stupid worthless garbage!"),
            FacebookPost("My dog is so cute, her name is Alia."),
            FacebookPost("Quit buying worthless dog food stupid"),
            FacebookPost("Mr Adams ate my steak, how to stop him")
        ) zip List(0, 1, 0, 1, 0)

    def splitEachText() = 
        loadDataSet.map((data) => data._1.text.split(" ").toList)

}