// import Math._

// case class House(lat: Int, long: Int)

// object Happiness {
//     def main(args: Array[String]) = {
//         val house1 = House(10, 10)
//         val result = kItems(house1, inputData)
//         println(result)
//     }

//     def inputData = Map(
//         House(56, 2) -> true,
//         House(3, 20) -> false,
//         House(18, 1) -> true,
//         House(20, 14) -> false,
//         House(30, 30) -> true,
//         House(35, 35) -> true
//     )

//     def calculateEucledianDIstance = (input: House) => {
//         val dataSet = inputData
//         dataSet.map((house) => {
//             sqrt( pow((house._1.lat - input.lat), 2) + pow((house._1.long - input.long), 2) )
//         })
//     }

//     def kItems = (input: House, dataSet: Map[House, Boolean]) => {
//         val result = calculateEucledianDIstance(input)
//         val k = getClasses(dataSet).size + 1
//         val combined = result zip dataSet.map((item) => item._2)
//         combined.sortBy((x) => x._2) take k
//     }

//     def getClasses = (dataSet: Map[House, Boolean]) => {
//         val classes: List[Boolean] = dataSet.map((item) => item._2).toList
//         classes.distinct
//     }

//     def classify = (dataSet: Map[House, Boolean]) => {
//         val classes: List[Boolean] = getClasses(dataSet)
//         classes
//     }
// }