import io._
import Math._

object titanic {

    case class CSVFile(filename: String)


    def main(args: Array[String]) {

        val testData = Vector("308", "1", "1", "Penasco", "Musa", "female", "17", "1", "0", "PC", "17758", "108.9", "C65")
        val testData2 = Vector("722", "0", "3", "Jensen, Mr. Svend", "Lauritz", "male", "17", "1", "0", "350048", "7.0542", "", "S")
        val testData3 = Vector("533", "0", "3", "Elias", "Mr. Joseph Jr", "male", "17", "1", "1", "2690", " 7.2292", "", "C")
        val testData4 = describeCSV(CSVFile("test.csv"))
            // println(describeCSV(CSVFile("test.csv")).toList)
            // println(testData4)
        println(classify(testData4))

        // println(describeCSV(CSVFile("test.csv")))
        // describeCSV(CSVFile("test.csv"))
    }

    def getData(filename: String):
        Vector[String] =
            Source.fromInputStream(getClass().getClassLoader()
                .getResourceAsStream(filename)).getLines.toVector.drop(1)

        def describeCSV(file: CSVFile): Array[Array[String]] = {
            val fileContents: Vector[String] = getData(file.filename)

                val nrows: Int = fileContents.length
            val ncols: Int = fileContents(0).split(",").length
            val rows = Array.ofDim[String](nrows, ncols)

            for ((line, count) < -getData(file.filename).zipWithIndex) {
                rows(count) = line.split(",").map(_.trim)
            }

            displayCSV(nrows, ncols, rows)

            rows

        }

        def checkIfNull(x: String): Double = x match {
            case a:
                String
                if (a == "") => 0
            case _ => x.toDouble
        }

        // def calculateDistance(data: Vector[String]):Array[Double] = {
        def calculateDistance(data: Array[Array[String]]): List[Double] = {

            var result: List[Double] = List()
            for (test < -describeCSV(CSVFile("test.csv"))) {

                val data6 = checkIfNull(test(6))
                val data7 = checkIfNull(test(10))

                for (train < -describeCSV(CSVFile("train.csv"))) {
                    val x6 = checkIfNull(train(6))
                    val x7 = checkIfNull(train(10))

                    val current: List[Double] = List(sqrt(pow((data6.toDouble - x6.toDouble), 2) + pow((data7.toDouble - x7.toDouble), 2)))
                    result = result::: current
                }
            }

            result

            // describeCSV(CSVFile("test.csv")).map((test) => {
            //     val data6 = checkIfNull(test(6))
            //     val data7 = checkIfNull(test(10))

            //     describeCSV(CSVFile("train.csv")).map((train) => {
            //         val x6 = checkIfNull(train(6))
            //         val x7 = checkIfNull(train(10))

            //         sqrt( pow((data6.toDouble - x6.toDouble), 2) + pow((data7.toDouble - x7.toDouble), 2) )
            //     })
            // })
        }

        def kItems(data: Array[Array[String]]) = {
            val newDataSet = calculateDistance(data)
            val k = round((calculateDistance(data).length / 3))
            // newDataSet.sortBy((x) => x._1) take 3
            newDataSet take 3
        }

        def classify(data: Array[Array[String]]) = {
            val newDataSet = kItems(data)

            newDataSet
            // val survive_count = newDataSet.filter((x) => x._2.split(",")(1) == "1").length
            // val no_survive_count = newDataSet.filter((x) => x._2.split(",")(1) == "0").length

            // if (survive_count > no_survive_count)
            //     return "Will Survive" 
            // else 
            //     return "Not Survive"
        }

        def displayCSV(nrows: Int, ncols: Int, rows: Array[Array[String]]) = {
            for (i < -0 until nrows) {
                var tempList: List[String] = List()
                for (j < -0 until ncols) {
                    val current: List[String] = List(rows(i)(j) + " | ")
                    tempList = tempList::: current
                }
                println(vectorToString(tempList))
            }
        }

        def vectorToString(vector: List[String]): String = {
            var string: String = ""
            for (i < -vector) {
                string += i
            }
            string
        }

}