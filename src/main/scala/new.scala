import scalaj.http.Http
import scala.util.parsing.json._

object news {
    val url = "https://api.github.com/search/repositories"
    val result = Http(url).param("q", "scala").asString

    def main(args: Array[String]) {
        println(result)
    }
}

