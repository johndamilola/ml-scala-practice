import Math._
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.Map

object Pearson {
    def main(args: Array[String]): Unit = {
        println( calculatePearson("Lisa Rose", "Gene Seymour") )
        println( topMatches("Toby") )
        println( getRecommendations("Toby") )
    }

    def typeOf[T: Manifest](t: T): Manifest[T] = manifest[T]

    // calculate the pearson correlation co-efficient
    def calculatePearson(person1: String, person2: String): Double = {
        val data = getCriticsData()
        var similarItems: Map[String, Boolean] = Map()

        data(person1) foreach ( (movie) => 
            if (data(person2).contains(movie._1)) similarItems += (movie._1 -> true)
        )

        val n = similarItems.size

        if (n == 0) System.exit(1)

        // Add up all the ratings of each users
        val sum1 = similarItems.keys.foldLeft(0.0)( _ + data(person1)(_) )
        val sum2 = similarItems.keys.foldLeft(0.0)( _ + data(person2)(_) )

        // sum up the squares
        val sum1Sq = similarItems.keys.foldLeft(0.0)( (acc, movie_title) => acc + pow((data(person1)(movie_title)), 2) )
        val sum2Sq = similarItems.keys.foldLeft(0.0)( (acc, movie_title) => acc + pow((data(person2)(movie_title)), 2) )

        //sum up the products
        val pSum = similarItems.keys.foldLeft(0.0)( (acc, movie_title) => acc + ( data(person1)(movie_title) * data(person2)(movie_title) ) )

        // Calculate Pearson score
        val num = pSum - (sum1 * sum2 / n)
        val den = sqrt( (sum1Sq - pow(sum1, 2) / n) * (sum2Sq - pow(sum2, 2) / n) )

        if (den == 0) System.exit(1)

        val r = num / den

        return r

    }

    // Get two people who match your movie taste
    def topMatches(person1: String) = {
        val data = getCriticsData()
        val topMatches: ListBuffer[Tuple2[String, Double]] = ListBuffer()

        val others = data.keys.filter((name) => name != person1)

        others.foreach((name) => {
            val regression = calculatePearson(person1, name)
            topMatches += Tuple2(name, regression)
        })

        topMatches.sortBy((x) => x._2).reverse take 2
    }

    // get all the movie list
    def getMovieList():ListBuffer[String] = {
        val allMovies: ListBuffer[String] = ListBuffer()

        getCriticsData().foreach((person_name) => {
            getCriticsData()(person_name._1).foreach((movies_title) => {
                allMovies += movies_title._1
            })
        })

        allMovies
    }

    def getRecommendations(person1: String) = {
        val data = getCriticsData()
        val similarity = topMatches(person1).filter((x) => x._2 > 0)

        val allMovies = getMovieList()
        val watchedMovies: ListBuffer[String] = ListBuffer()
        val totals: Map[String, Map[String, Double]] = Map()

        data(person1).foreach((movies_title) => {
            watchedMovies += movies_title._1
        })

        val unwatchedMovies = allMovies.filterNot(watchedMovies.contains(_)).distinct
        
        // don't compare with myself
        val others = data.keys.filter((name) => name != person1)


        unwatchedMovies.foreach((movie_title) => {
            others.foreach((person2) => {
                if (data(person2).contains(movie_title)) {
                    println("Hello")
                    // val sim = calculatePearson(person1, person2)
                    // val score = data(person2)(movie_title)

                    // totals(movie_title)(person2) = (sim * score)
                }
            })
        })


        // others.foreach((person2) => {
        //     val sim = calculatePearson(person1, person2)
        //     println("sim" + sim)
        //     // ignore scores of zero and lower
        //     if (sim > 0) {
        //         unwatchedMovies.foreach((movie_title) => {
                    
        //             totals(movie_title) = Map(sim * movie_title)
        //         })

        //         data(person2).foreach((movie_title) => {
        //             if (unwatchedMovies.contains(movie_title._1)) {
        //                 // similarity * Score
        //                 println("score" + movie_title._2)
        //                 totals(movie_title._1) = sim * movie_title._2
        //             }
        //         })
        //     }
                
        // })

        // unwatchedMovies
        totals
    }

    def getCriticsData(): Map[String, Map[String, Double]] = Map(
        "Lisa Rose" -> Map(
            "Lady in the Water" -> 2.5,
            "Snakes on a Plane" -> 3.5, 
            "Just My Luck" -> 3.0, 
            "Superman Returns" -> 3.5, 
            "You, Me and Dupree" -> 2.5,
            "The Night Listener" -> 3.0
        ),
        "Gene Seymour" -> Map(
            "Lady in the Water" -> 3.0, 
            "Snakes on a Plane" -> 3.5,
            "Just My Luck" -> 1.5,
            "Superman Returns" -> 5.0,
            "The Night Listener" -> 3.0,
            "You, Me and Dupree" -> 3.5
        ),
        "Michael Phillips" -> Map(
            "Lady in the Water" -> 2.5,
            "Snakes on a Plane" -> 3.0,
            "Superman Returns" -> 3.5, 
            "The Night Listener" -> 4.0
        ),
        "Claudia Puig" -> Map(
            "Snakes on a Plane" -> 3.5, 
            "Just My Luck" -> 3.0,
            "The Night Listener" -> 4.5, 
            "Superman Returns" -> 4.0,
            "You, Me and Dupree" -> 2.5
        ),
        "Mick LaSalle" -> Map(
            "Lady in the Water" -> 3.0, 
            "Snakes on a Plane" -> 4.0,
            "Just My Luck" -> 2.0, 
            "Superman Returns" -> 3.0, 
            "The Night Listener" -> 3.0,
            "You, Me and Dupree" -> 2.0
        ),
        "Jack Matthews" -> Map(
            "Lady in the Water" -> 3.0, 
            "Snakes on a Plane" -> 4.0,
            "The Night Listener" -> 3.0, 
            "Superman Returns" -> 5.0, 
            "You, Me and Dupree" -> 3.5
        ),
        "Toby" -> Map(
            "Snakes on a Plane" -> 4.5,
            "You, Me and Dupree" -> 1.0,
            "Superman Returns" -> 4.0
        )
    )
}