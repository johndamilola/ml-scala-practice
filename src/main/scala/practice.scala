import java.util.Date

sealed trait Visitor {
    def id: Long
    def createdAt: Date

    def age: Long = new Date().getTime - createdAt.getTime
}

final case class Anonymous(id: Long, createdAt: Date = new Date() ) extends Visitor

final case class User(id: Long, email: String, createdAt: Date = new Date() ) extends Visitor

object practice {
    def main(args: Array[String]) = {
        val result = older(Anonymous(1), User(2, "test@example.com"))

        val person1 = Anonymous(3)
        // Thread.sleep(5000)
        println(person1.age)

        println(result)

        println(describe(person1))

        println(divide(34,2))

        println(divide(1, 0) match {
            case Finite(value) => s"It's finite: ${value}"
            case Infinite      => s"It's infinite"
        })
    }

    def older(v1: Visitor, v2: Visitor): Boolean =
        v1.createdAt.before(v2.createdAt)
        
    def describe(v: Visitor): String = v match {
        case User(_, _, _) => "Registered User!"
        case Anonymous(_, _) => "Anonymous!"
    }
    
}

sealed trait DivisionResult
final case class Finite(result: Int) extends DivisionResult
final case object Infinite extends DivisionResult

object divide {
    def apply(num1: Int, num2: Int): DivisionResult = num2 match {
        case 0 => Infinite
        case _ => Finite(num1 / num2)
    }
}