sealed trait Animal {
    def dinner: Food = this match {
        case Lion() => Antelope
        case Tiger() => TigerFood
        case Panther() => Licorice
        case Cat(favouriteFood) => CatFood(favouriteFood)
    }
}
final case class Lion() extends Animal
final case class Tiger() extends Animal
final case class Panther() extends Animal
final case class Cat(favouriteFood: String) extends Animal

sealed trait Food
final case object Antelope extends Food
final case object TigerFood extends Food
final case object Licorice extends Food
final case class CatFood(food: String) extends Food