import scala.collection.mutable.ListBuffer
import Math._

object shannon {

    case class DataPoint(no_surfacing: Int, flippers: Int, label: String)


    def main(args: Array[String]) {

        println(createDataSet())

        println(getClassesCount(createDataSet()))

        println(calcShannonEntropy())

    }

    def createDataSet():List[DataPoint] = 
        List(
            DataPoint(1, 1, "Yes"),
            DataPoint(1, 1, "Yes"),
            DataPoint(1, 0, "Maybe"),
            DataPoint(0, 1, "No"),
            DataPoint(0, 1, "No")
        )

    def getClassesCount(dataSet: List[DataPoint]): ListBuffer[(String, Double)] = {
        val classes: ListBuffer[String] = ListBuffer()
        val count: ListBuffer[Double] = ListBuffer()

        dataSet.foreach(data => {
            if (classes.length == 0)
                classes += (data.label)
            else {
                if (classes.filter(x => data.label == x).isEmpty) {
                    classes += (data.label)
                }
            }
        })

        classes.foreach(x => { count += dataSet.filter(s => s.label == x).length })

        val prob = count.map(x => x/5)
        classes zip prob
    }

    def calcShannonEntropy(): Double = {
        var result: Double = 0.0

        getClassesCount(createDataSet()).map(x => {
            result -= x._2 * log(x._2) / log(2)
        })

        result

    }

        

}

