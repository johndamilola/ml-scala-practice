import io._
import Math._

object titanic {

    case class CSVFile(filename: String)


    def main(args: Array[String]) {

        val testData = CSVFile("train.csv")
        
        allClassify(testData)

    }

    def getData(filename: String): Vector[String] = 
        Source.fromInputStream(getClass().getClassLoader()
            .getResourceAsStream(filename)).getLines.toVector.drop(1)

    def describeCSV(file: CSVFile, data_type: String): Array[Array[String]] = {
        val fileContents: Vector[String] = getData(file.filename)

        val nrows: Int = fileContents.length
        val ncols: Int = fileContents(0).split(",").length
        val rows = Array.ofDim[String](nrows, ncols)

        for ((line, count) <- getData(file.filename).zipWithIndex)  {
            rows(count) = line.split(",").map(_.trim)
        }

        // displayCSV(nrows, ncols, rows)

        var result:Array[Array[String]] = Array()
        if (data_type == "TRAIN")
            result = rows take (rows.length / 2)
        else if (data_type == "TEST")
            result = rows drop (rows.length / 2)

        result

    }

    def checkIfNull(x: String): Double = x match {
        case a: String if (a == "") => 1
        case _ => x.toDouble
    }

    def calculateDistance(data: Array[String]): Array[Double] = {

        describeCSV(CSVFile("train.csv"), "TRAIN").map((train) => {
            val x6 = checkIfNull(train(6))
            val x7 = checkIfNull(train(10))

            val data6 = checkIfNull(data(6))
            val data7 = checkIfNull(data(10))

            sqrt( pow((data6.toDouble - x6.toDouble), 2) + pow((data7.toDouble - x7.toDouble), 2) )
        })

    }

    def kItems(data: Array[String]) = {
        val newDataSet = calculateDistance(data) zip getData("train.csv")
        val k = round((calculateDistance(data).length / 3))
        newDataSet.sortBy((x) => x._1) take 1
    }

    def classify(data: Array[String]): Unit  = {
        val newDataSet = kItems(data)
        var result: List[(String, Int)] = List()
        
        val survive_count = newDataSet.filter((x) => {
            val check = checkIfNull(x._2.split(",")(1)) 
            check == 1
        }).length
        val no_survive_count = newDataSet.filter((x) => {
            val check = checkIfNull(x._2.split(",")(1)) 
            check == 0
        }).length
        
        if (survive_count > no_survive_count) 
            println(data(0) + ", " + 1 )
        else if (survive_count < no_survive_count) {
            println(data(0) + ", " + 0 )
        } else
            println(data(0) + ", " + "Not sure" )
    }

    def allClassify(file: CSVFile) = {
        describeCSV(file, "TEST").foreach((test) => {
            classify(test)
        })
    }

    def displayCSV(nrows: Int, ncols: Int, rows: Array[Array[String]]) = {
        for (i <- 0 until nrows) {
            var tempList:List[String] = List()
            for (j <- 0 until ncols) {
                val current:List[String] = List(rows(i)(j) + " | ")
                tempList = tempList ::: current
            }
            println(vectorToString(tempList))
        }
    }

    def vectorToString(vector: List[String]): String = {
        var string: String = ""
        for (i <- vector) {
            string += i
        }
        string
    }

}
