import io._
import scala.collection.mutable.ListBuffer
import scala.util.Try

object studentIntervention {

    case class CSVFile(filename: String)

    def main(args: Array[String]) {
        // 1. EXPLORING THE DATA

        // Read student data
        val student_data = getData("student-data.csv")

        // TODO: Calculate number of students
        val no_student = student_data.length - 1

        // TODO: Calculate number of features
        val no_features = student_data.head.split(",").length

        // TODO: Calculate passing students
        val passed_index = student_data.head.split(",").indexOf("passed")
        val no_passed = student_data.drop(1).filter((row) => row.split(",")(passed_index) == "yes").length

        // TODO: Calculate failing students
        val no_failed = student_data.drop(1).filter((row) => row.split(",")(passed_index) == "no").length

        // TODO: Calculate graduation rate
        val grad_rate: Double = no_passed * 100 / no_student

        println(s"Total number of students: $no_student")
        println(s"Number of features: $no_features")
        println(s"Number of students who passed: $no_passed")
        println(s"Number of students who failed: $no_failed")
        println(s"Graduation rate of the class: $grad_rate")

        // 2. PREPARING THE DATA
        val feature_cols = student_data.head.split(",").init.toList
        val target_col = student_data.head.split(",").last

        println(s"Feature columns: $feature_cols")
        println(s"Target column: $target_col")


        // Separate the data into feature data and target data (X_all and y_all, respectively)
        val X_all = student_data.map((row) => row.split(",").init.toArray)
        val y_all = student_data.map((row) => row.split(",").last)

        println("Feature values")
        println(X_all.tail take 2)
        println("Targer values")
        println(y_all.tail take 2)

        println(checkColumnIfCategorical(X_all, 0))
        println(makeNewHeaders(X_all).length)
    }

    def typeOf[T: Manifest](t: T): Manifest[T] = manifest[T]

    def getData(filename: String): Vector[String] = 
        Source.fromInputStream(getClass().getClassLoader()
            .getResourceAsStream(filename)).getLines.toVector
 
    def makeNewHeaders(student_data: Vector[Array[String]]) = {
        student_data.head.toList.zipWithIndex.map { case (element, index) => 
            checkColumnIfCategorical(student_data, index)
        }.flatten
    }

    def checkColumnIfCategorical(student_data: Vector[Array[String]], colNumber: Int): List[String] = {
        // grab the column and convert to a List
        val column = student_data.tail.map(line => {
            line(colNumber)
        })

        //check if it is category
        val distinctCol = column.distinct
        
        val colTitle = distinctCol.toList match {
            case y if ((Try(y(0).toDouble).isSuccess) == true) => List(student_data.head(colNumber))
            case y if (y.contains("yes") && y.contains("no") == true) => List(student_data.head(colNumber))
            case _ => distinctCol.toList.map(x => student_data.head(colNumber) + x)
        }

        if (distinctCol.length > 1) colTitle
        else colTitle
    }

}